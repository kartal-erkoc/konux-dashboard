import React, { Component } from "react";
import NavItem from "./NavItem.jsx";
import { statics } from "../../common/static/statics.json";

class NavMenu extends Component {
  state = {
    activeIndex: 0
  };

  render() {
    let activeIndex = this.state.activeIndex;

    return (
      <div className="navmenu">
        <ul className="navmenu__list">
          {statics.navigationItems.map((item, i) => (
            <NavItem
              label={item.label}
              itemUrl={item.url}
              key={item.label}
              index={item.index}
              active={item.index === activeIndex}
            />
          ))}
        </ul>
      </div>
    );
  }
}

export default NavMenu;
