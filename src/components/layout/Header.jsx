import React, { Component } from "react";
import logo from "../../assets/images/logo/konux_logo.png";
import NavMenu from "./NavMenu.jsx";

class Header extends Component {
  render() {
    return (
      <header className="header">
        <div className="header__logo-container">
          <img className="header__logo" src={logo} alt="logo" />
        </div>
        <NavMenu />
      </header>
    );
  }
}

export default Header;
