import React, { Component } from "react";

class Navitem extends Component {
  getClassName = active => {
    let element = "navmenu__list-item ";
    let modifier = active ? "navmenu__list-item--active" : "";
    let className = element + modifier;
    return className;
  };

  render() {
    const { label, itemUrl, active } = this.props;
    let className = this.getClassName(active);

    return <li className={className}>{label}</li>;
  }
}

export default Navitem;
