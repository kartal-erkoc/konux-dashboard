import React, { Component } from "react";

class Button extends Component {
  render() {
    const { label, onClick = null } = this.props;

    return (
      <button className="button" onClick={onClick}>
        {label}
      </button>
    );
  }
}

export default Button;
