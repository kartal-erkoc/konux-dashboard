import React, { Component } from "react";

class YAxis extends Component {
  render() {
    const { innerHeight, innerWidth, yLabel, yLabelOffset, data } = this.props;

    return (
      <g className="y axis">
        <text
          className="label"
          transform={`translate(-${yLabelOffset}, ${innerHeight /
            2}) rotate(-90)`}
        >
          {yLabel}
        </text>
      </g>
    );
  }
}

export default YAxis;
