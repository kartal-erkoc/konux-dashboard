import React, { Component } from "react";
import d3 from "d3";

class XAxis extends Component {
  getXScale = innerWidth => {
    return d3.time.scale().range([0, innerWidth]);
  };

  getXAxis = xScale => {
    let xAxis = d3.svg
      .axis()
      .scale(xScale)
      .orient("bottom")
      .ticks(10)
      .outerTickSize(0);
    return xAxis;
  };

  componentDidMount = () => {
    let xAxisG = d3
      .select("svg")
      .select("g")
      .select("g");

    let xScale = this.getXScale(this.props.innerWidth).domain(
      d3.extent(this.props.data, function(d) {
        return d.x;
      })
    );
    let xAxis = this.getXAxis(xScale);
    xAxisG.call(xAxis);
  };

  render() {
    const { innerHeight, innerWidth, xLabel, xLabelOffset, data } = this.props;

    return (
      <g className="x axis" transform={`translate(0, ${innerHeight})`}>
        <text
          className="label"
          style={{ textAnchor: "middle" }}
          transform={`translate(${innerWidth / 2}, ${xLabelOffset})`}
        >
          {xLabel}
        </text>
      </g>
    );
  }
}

export default XAxis;
