import React, { Component } from "react";
import Line from "./Line.jsx";
import XAxis from "./XAxis.jsx";
import YAxis from "./YAxis.jsx";

class Chart extends Component {
  getInnerDimension = (outerDimension, margin, oppositeMargin) => {
    let innerDimension = outerDimension - margin - oppositeMargin;
    return innerDimension || 0;
  };

  formatData = (data, xColumn, yColumn) => {
    let formattedData = data.values.map(d => ({
      x: new Date(d[xColumn]),
      y: d[yColumn]
    }));
    return formattedData;
  };

  sortData = data => {
    if (!data) {
      return null;
    }
    let sortedData = [...data];
    sortedData.sort(function(a, b) {
      return a.x - b.x;
    });
    return sortedData;
  };

  render() {
    const {
      data,
      outerWidth = 0,
      outerHeight = 0,
      marginTop = 0,
      marginRight = 0,
      marginBottom = 0,
      marginLeft = 0,
      xColumn = "x",
      xLabel = "",
      xLabelOffset = 48,
      yColumn = "y",
      yLabel = "",
      yLabelOffset = 40,
      fill = "none",
      stroke = "black",
      strokeWidth = "1px"
    } = this.props;

    let dataGiven = data && data.values && data.values.length;
    let dimensionsGiven = outerWidth && outerHeight;

    let innerWidth = this.getInnerDimension(
      outerWidth,
      marginLeft,
      marginRight
    );
    let innerHeight = this.getInnerDimension(
      outerHeight,
      marginTop,
      marginBottom
    );
    let formattedData = dataGiven
      ? this.formatData(data, xColumn, yColumn)
      : null;
    let sortedData = this.sortData(formattedData);

    return (
      dataGiven &&
      dimensionsGiven && (
        <svg width={outerWidth} height={outerHeight}>
          <g transform={`translate(${marginLeft}, ${marginTop})`}>
            <Line
              fill={fill}
              stroke={stroke}
              strokeWidth={strokeWidth}
              data={sortedData}
              innerWidth={innerWidth}
              innerHeight={innerHeight}
            />
            <XAxis
              innerHeight={innerHeight}
              innerWidth={innerWidth}
              xLabel={xLabel}
              xLabelOffset={xLabelOffset}
              data={sortedData}
            />
            <YAxis
              innerHeight={innerHeight}
              innerWidth={innerWidth}
              yLabel={yLabel}
              yLabelOffset={yLabelOffset}
              data={sortedData}
            />
          </g>
        </svg>
      )
    );
  }
}

export default Chart;
