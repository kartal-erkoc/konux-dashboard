import React, { Component } from "react";
import d3 from "d3";

class Line extends Component {
  getXScale = innerWidth => {
    return d3.time.scale().range([0, innerWidth]);
  };

  getYScale = innerHeight => {
    return d3.scale.linear().range([innerHeight, 0]);
  };

  getLine = (xScale, yScale) => {
    return d3.svg
      .line()
      .x(function(d) {
        return xScale(d.x);
      })
      .y(function(d) {
        return yScale(d.y);
      });
  };

  getDAttribute = (data, line) => {
    return line(data);
  };

  render() {
    const {
      data,
      fill,
      stroke,
      strokeWidth,
      innerWidth,
      innerHeight
    } = this.props;
    let xScale = this.getXScale(innerWidth);
    let yScale = this.getYScale(innerHeight);
    xScale.domain(
      d3.extent(data, function(d) {
        return d.x;
      })
    );
    yScale.domain(
      d3.extent(data, function(d) {
        return d.y;
      })
    );
    let line = this.getLine(xScale, yScale);
    let dAttribute = this.getDAttribute(data, line);

    return (
      <path
        fill={fill}
        stroke={stroke}
        strokeWidth={strokeWidth}
        d={dAttribute}
      />
    );
  }
}

export default Line;
