import * as http from "axios";

export function requestData(requestType, path) {
  return new Promise((resolve, reject) => {
    http({
      url: path,
      method: requestType
    })
      .then(response => {
        resolve(response);
      })
      .catch(error => {
        reject(error);
      });
  });
}
