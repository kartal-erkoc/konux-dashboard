import React, { Component } from "react";
import LineChart from "../components/graphics/LineChart.jsx";
import Header from "../components/layout/Header.jsx";
import Button from "../components/action/Button.jsx";
import { endpoints } from "../common/static/endpoints.json";
import { requestData } from "../common/scripts/services/networkServices";

class Dashboard extends Component {
  state = {
    data: null
  };

  onDataRequested = () => {
    let path = endpoints.CHART_DATA;

    requestData("get", path)
      .then(response => {
        if (response && response.data && response.data.values) {
          let chartData = response.data;
          this.setState({
            data: chartData
          });
        }
      })
      .catch(e => {
        console.log(e);
      });
  };

  render() {
    let data = this.state.data;
    const outerWidth = 500,
      outerHeight = 250;
    const marginTop = 5,
      marginRight = 90,
      marginBottom = 60,
      marginLeft = 70;

    return (
      <div className="dashboard">
        <Header />
        <div>
          <Button onClick={this.onDataRequested} label={"Generate chart"} />
        </div>

        <LineChart
          data={data}
          outerWidth={outerWidth}
          outerHeight={outerHeight}
          marginTop={marginTop}
          marginRight={marginRight}
          marginBottom={marginBottom}
          marginLeft={marginLeft}
          xLabel={"Time"}
          yLabel={"Values"}
        />
      </div>
    );
  }
}

export default Dashboard;
