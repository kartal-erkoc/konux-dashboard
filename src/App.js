import React, { Component } from "react";
import Dashboard from "./pages/Dashboard.jsx";
import "./assets/sass/App.css";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Dashboard />
      </div>
    );
  }
}

export default App;
