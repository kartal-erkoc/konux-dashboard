# Brief introduction about the project

  - This project involves client side application for a chart visualization using React and d3.js
  - For styling, SASS has been used as a CSS preprocessor along with BEM architecture when applicable
  - Although both React and d3.js is built upon DOM manipulation, d3.js is used just for math calculations on chart data and React is used for creating DOM elements as it deals with performance optimization using its Virtual DOM
  - About React folder structure, reusable components are divided into related folders under the components folder. Page specific components are placed under the pages folder
  - About CSS folder structure, 7-1 SASS folder structure has been implemented
  - To request data, Axios is preferred for the project

# Environment setup

  - To be able to start the project, first this git repository should be cloned into a desired local directory
  - When cloned, navigate into the directory via command prompt
  - Since React is being started by using NPM Scripts, NodeJS along with NPM should be installed
  - When installation completed, run "npm install" command while inside the cloned repository folder. This will install module dependencies required by the project
  - Finally, running "npm start" command will begin the project

